# Binary repo and shared Ivy configuration for data-bag.org projects

### The goals are:

- to provide a single source of binary modules for <data-bag.org> projects
- ... and shared infrastructure to publish and obtain these modules with [Apache Ivy](https://ant.apache.org/ivy/)
- to keep this repo Maven-compatible