<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
 |  Copyright 2024 Stan Livitski
 |
 |  This file is free software: you can redistribute it and/or modify
 |  it under the terms of the GNU Affero General Public License as published by
 |  the Free Software Foundation, either version 3 of the License, or
 |  (at your option) any later version.
 |
 |  This file is distributed in the hope that it will be useful,
 |  but WITHOUT ANY WARRANTY; without even the implied warranty of
 |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 |  GNU Affero General Public License for more details.
 |
 |  You should have received a copy of the GNU Affero General Public License
 |  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 |
 |  Additional permissions under GNU Affero GPL version 3 section 7:
 |
 |  1. If you modify this file, or any covered work, by linking or combining
 |  it with any program covered by the terms of Apache License version 2.0,
 |  Common Public License version 1.0. the Data-bag Project License,
 |  Eclipse Public License version 1.0, and/or Eclipse Distribution License
 |  version 1.0, the licensors of this file grant you
 |  additional permission to convey the resulting work.
 |  Corresponding Source for a non-source form of such a
 |  combination shall include the source code for the aforementioned
 |  program as well as that of the covered work.
 |
 |  2. If you modify this Program, or any covered work, by linking or combining
 |  it with the Java Server Pages Expression Language API library (or a
 |  modified version of that library), containing parts covered by the terms of
 |  JavaServer Pages Specification License, the licensors of this Program grant
 |  you additional permission to convey the resulting work.
 |
 ====================================================================== -->
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output	method="xml" encoding="UTF-8" omit-xml-declaration="yes" />

<xsl:template match="/">
 <xsl:text>
</xsl:text>
 <xsl:copy-of select="comment()[1]" />
</xsl:template>

</xsl:transform>
